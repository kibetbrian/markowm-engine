'use strict';

var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],
    ['blockquote', 'code-block'],
    [{
        'header': [1, 2, 3, 4, 5, 6, false]
    }],
    [{
        'list': 'ordered'
    }, {
        'list': 'bullet'
    }],
    [{
        'script': 'sub',
    }, {
        'script': 'super',
    }],
    [{
        'indent': '-1',
    }, {
        'indent': '+1',
    }],
    [{
        'direction': 'rtl',
    }],
    // ['link', 'image', 'video', 'formula'],
];
var quill = new Quill('#editor', {
    modules: {
        toolbar: toolbarOptions
    },
    theme: 'snow'
});

function logHtmlContent() {
    console.log(quill.root.innerHTML);
}