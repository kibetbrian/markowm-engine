## About Markdown HTML Engine

When you type a word or a sentense on the markdown engine input converts the text to an html text according to the syntax you've selected .

## Installation & Running The File

1. Clone the project and run the html file on your browser
2. Type any word or sentence on the input field.
3. Open your console.
4. Then click the button "Log content to Console".
5. Read your results from the console.
